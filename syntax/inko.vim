" Vim syntax file
" Language: Inko

if exists("b:current_syntax")
  finish
endif
let b:current_syntax = "inko"

" Inko's "struct/enum/etc" equivalents, roughly
syn keyword inkoStructure object trait impl for
syn keyword inkoStatement return try else throw
syn keyword inkoStorage let mut

" the Conditional trait, basically
syn keyword inkoConditional if if_true if_false and or
" the Block's looping methods, and Iterator's each
syn keyword inkoRepeat while_true while_false loop each

" Keywords not already in structure/statement
syn keyword inkoKeyword import self def do as lambda where
" Built-in types
syn keyword inkoType Boolean Nil Object String Integer Float Block Array Trait HashMap
" Boolean values
syn keyword inkoBoolean True False

" Operators
syn match inkoOperator "\v(\!|\%|\^|\&|\*|\-|\+|\=|\<|\>|\/|\|)"

" Numbers
syn match inkoNumber "\<[0-9_]\+\>"
syn match inkoHexNumber "\<0x[0-9a-fA-F_]\+\>"
syn match inkoFloatNumber "\<[0-9_]\+.[0-9_]\+\>"
syn match inkoFloatExpNumber "\<[0-9_]\+\%(.[0-9_]\+\)\?\%([Ee][+-]\?\)[0-9_]\+\>"

" Comments
syn region inkoCommentLine start="#" end="$" contains=inkoTodo,@Spell
syn keyword inkoTodo contained TODO

" Strings
syn match inkoEscape display contained /\\\([nrte"]\)/
syn match inkoSingleEscape display contained /\\'/
syn region inkoSingleQuoteString start=+'+ skip=+\\'+ end=+'+ oneline contains=inkoSingleEscape
syn region inkoDoubleQuoteString start=+"+ skip=+\\"+ end=+"+ oneline contains=inkoEscape

hi def link inkoCommentLine Comment
hi def link inkoTodo Todo
hi def link inkoHexNumber inkoNumber
hi def link inkoFloatNumber inkoNumber
hi def link inkoFloatExpNumber inkoNumber
hi def link inkoNumber Number
hi def link inkoKeyword Keyword
hi def link inkoType Type
hi def link inkoSingleQuoteString String
hi def link inkoDoubleQuoteString String
hi def link inkoSingleEscape inkoEscape
hi def link inkoEscape Special
hi def link inkoBoolean Boolean
hi def link inkoOperator Operator
hi def link inkoStructure Structure
hi def link inkoStatement Statement
hi def link inkoConditional Conditional
hi def link inkoRepeat Repeat
hi def link inkoStorage StorageClass
