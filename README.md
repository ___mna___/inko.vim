# inko.vim

File detection and syntax highlighting for the
[inko](https://gitlab.com/yorickpeterse/inko) programming language.

*Update*: there is now an official Vim plugin, it should be used instead: https://gitlab.com/inko-lang/inko.vim .

## Installation

For use with [vim-plug], use:

```
call plug#begin()
" ...
Plug 'https://gitlab.com/___mna___/inko.vim.git'
" ...
call plug#end()
```

## License

MIT, see LICENSE file. Based on [zig.vim].

[vim-plug]: https://github.com/junegunn/vim-plug
[zig.vim]: https://github.com/ziglang/zig.vim
